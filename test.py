import pytest
from main import TreeStore


items = [
    {"id": 1, "parent": "root"},
    {"id": 2, "parent": 1, "type": "test"},
    {"id": 3, "parent": 1, "type": "test"},
    {"id": 4, "parent": 2, "type": "test"},
    {"id": 5, "parent": 2, "type": "test"},
    {"id": 6, "parent": 2, "type": "test"},
    {"id": 7, "parent": 4, "type": None},
    {"id": 8, "parent": 4, "type": None}
]

ts = TreeStore(items)


class TestTreeStore:

    @pytest.mark.parametrize(
        "items_test",
        [
            items,
            [],
            [
                {"id": 1, "parent": "root"},
                {"id": 2, "parent": 1, "type": "test"},
                {"id": 3, "parent": 1, "type": "test"},
                {"id": 4, "parent": 2, "type": "test"},
                {"id": 5, "parent": 2, "type": "test"},
                {"id": 6, "parent": 2, "type": "test"},
                {"id": 7, "parent": 4, "type": None},
                {"id": 8, "parent": 4, "type": None},
                {"id": 9, "parent": 7, "type": None},
                {"id": 10, "parent": 7, "type": None},
                {"id": 11, "parent": 8, "type": None},
            ],
        ]
    )
    def test_get_all(self, items_test: list):
        ts_test = TreeStore(items)
        assert ts_test.get_all() == items

    @pytest.mark.parametrize(
        "node_id, res",
        [
            (3, {"id": 3, "parent": 1, "type": "test"}),
            (5, {"id": 5, "parent": 2, "type": "test"}),
            (111, None),
        ]
    )
    def test_get_item(self, node_id: int, res: dict):
        assert ts.get_item(node_id) == res

    @pytest.mark.parametrize(
        "node_id, res",
        [
            (1, [{"id": 2, "parent": 1, "type": "test"}, {"id": 3, "parent": 1, "type": "test"}]),
            (2, [{"id": 4, "parent": 2, "type": "test"}, {"id": 5, "parent": 2, "type": "test"},
                 {"id": 6, "parent": 2, "type": "test"}]),
            (5, []),
            (22, None),
        ]
    )
    def test_get_children(self, node_id: int, res: dict):
        assert ts.get_children(node_id) == res

    @pytest.mark.parametrize(
        "node_id, res",
        [
            (3, [{"id": 1, "parent": "root"}]),
            (1, []),
            (7, [{"id": 4, "parent": 2, "type": "test"}, {"id": 2, "parent": 1, "type": "test"},
                 {"id": 1, "parent": "root"}]),
            (5, [{"id": 2, "parent": 1, "type": "test"}, {"id": 1, "parent": "root"}]),
            (111, None),
        ]
    )
    def test_get_all_parents(self, node_id: int, res: dict):
        assert ts.get_all_parents(node_id) == res
