class TreeStore:
    def __init__(self, items_list: list):
        self.items = items_list
        self.tree = {}
        for item in items_list:
            parent_item = self.tree.get(item["parent"], None)
            if parent_item is None:
                self.tree.setdefault(item["parent"], {"item": None, "children": item})
            else:
                self.tree[item["parent"]]["children"].append(item)
            self.tree.setdefault(item["id"], {"item": item, "children": []})

    def get_all(self) -> list[dict]:
        """Возвращает изначальный список элементов"""
        return self.items

    def get_item(self, node_id: int) -> dict | None:
        """Возвращает элемент по его id"""
        node = self.tree.get(node_id)
        return None if node is None else node["item"]

    def get_children(self, node_id: int) -> list[dict] | list:
        """Возвращает детей по id родителя"""
        node = self.tree.get(node_id)
        return None if node is None else node["children"]

    def get_all_parents(self, node_id: int) -> list[dict] | None:
        """Возвращает всех родительских элементов от самого элемента до корня дерева"""
        node = self.get_item(node_id)
        if node is None:
            return None
        parents = []
        current_id = node_id
        while current_id in self.tree:
            item = self.tree[current_id]["item"]
            parent_id = None if item is None else item['parent']
            if parent_id not in self.tree:
                break
            item = self.tree[parent_id]["item"]
            if item is not None:
                parents.append(item)
            current_id = parent_id
        return parents
